
public class MyFour<T> {
	
	private T item1;
	private T item2;
	private T item3;
	private T item4; 
	
	public MyFour(T item1,T item2,T item3,T item4) 
	{
		super();
		
		this.item1=item1;
		this.item2=item2;
		this.item3=item3;
		this.item4=item4;
		
	}

	public boolean allEqual()
	{
		boolean flag=false;
		if(item1.equals(item2)==true && item1.equals(item3)==true && item1.equals(item4)==true)
		{
			flag=true;
		}
		else
		{
			flag=false;
		}
		
		return flag;
	}
	
	public void shiftLeft()
	{
		T temp;
		temp=item4;
		item4=item1;
		item1=item2;
		item2=item3;
		item3=temp;
		
		System.out.println("After shifted: "+item1+","+item2+","+item3+","+item4);//newly committed
	}
	
	@Override
	public String toString() {
		return item1 + ","  + item2 + "," + item3 + "," + item4;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//MyFour <String> myfourString=new MyFour<String>("Dhivya","Dhivya","Dhivya","Dhivya");
		MyFour <String> myfourString=new MyFour<String>("Dhivya","Selvaraj","Rita","Naveen");
		
		//MyFour <Integer> myfourInteger=new MyFour<Integer>(1,1,1,1);
		MyFour <Integer> myfourInteger=new MyFour<Integer>(1,2,3,4);
		

		System.out.println("String objects are "+myfourString.toString());
		
		
		boolean itemsStr=myfourString.allEqual();
		System.out.println(itemsStr);
		if(itemsStr)
		{
		System.out.println("**All String objects are equal**");
		}
		
		else
		{
			System.out.println("**String objects are not equal**");
		}
		
		myfourString.shiftLeft();
		
		System.out.println("***************");
		
		System.out.println("Integer objects are "+myfourInteger);

		boolean itemsInt=myfourInteger.allEqual();
		System.out.println(itemsInt);
		if(itemsInt)
		{
		System.out.println("**All Integer objects are equal**");
		}
		
		else
		{
			System.out.println("**Integer objects are not equal**");
		}
		myfourInteger.shiftLeft();
		
		
	}

}
