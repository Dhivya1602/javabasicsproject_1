
/* *************************************************
 *                                                  
 *  File       : Main.java                         
 *  Author     : Dhivya Selvaraj                   
 *  Date       : 09-04-2020                        
 *  Description: This file EvaluateTemperature()   
 *  gets the user input 'temperature' either in     
 *  Celsius or Fahrenheit and convert to Fahrenheit 
 *  if given input is Celsius and check for         
 *  the respective climate conditions and print.    
 *                                                                                                              
 * *************************************************/

import java.util.Scanner;

public class EvaluateTemperature {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the temperature:");
		int temp = sc.nextInt();/* To get the user input for temperature */

		System.out.println("Enter 'C' for Celsius and 'F' for Fahrenheit");
		sc.nextLine();
		String tempDenotion = sc
				.nextLine();/* To get the user input for Celsius/Fahrenheit */
		double Fh = 0;/* The variable Fh is to store the temperature value in Fahrenheit */
						 

		try {
			if (tempDenotion.equalsIgnoreCase("C")) {
				Fh = (temp * 1.8) + 32;// Converting Celsius to Fahrenheit and stored in Fh
									

			} else {
				Fh = temp;
			}
			// According to the 'Fh' value the respective output line will get printed.

			if (Fh < 0) {
				System.out.println("Extremely cold");
			} else if (Fh >= 0 && Fh <= 32) {
				System.out.println("Very cold");
			} else if (Fh >= 33 && Fh <= 50) {
				System.out.println("Cold");
			} else if (Fh >= 51 && Fh <= 70) {
				System.out.println("Mild");
			} else if (Fh >= 71 && Fh <= 90) {
				System.out.println("Warm");
			} else if (Fh >= 91 && Fh <= 100) {
				System.out.println("Hot");
			} else {
				System.out.println("Very Hot");
			}
		} finally {
			sc.close();
		}
	}

}
